package cn.chowa.ejyy.service;


import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.utils.SendHttpRequestUtil;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.json.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Map;


/**
 * @ClassName WeChat
 * @Description TODO
 * @Author ironman
 * @Date 11:08 2022/8/25
 */
public class WeChat {



    @Value("${config.wechat.ump.appid}")
    private static String umpAppId;
    @Value("${config.wechat.ump.secret}")
    private static String umpSecret;


    public static String userMpAccessToken = null;//默认为空
    public static long userMpAccessTokenExpire = 0;
    public static long userMpAccessTokenStartAt = 0;


    /**
     * 获取 accessToken
     */
    public static String getUserMpAccessToken()  {
        if (userMpAccessToken == null || (Calendar.getInstance().getTimeInMillis() - userMpAccessTokenStartAt) >= userMpAccessTokenExpire) {
            userMpAccessTokenStartAt = Calendar.getInstance().getTimeInMillis();
//            String url = "https://api.weixin.qq.com/cgi-bin/token?appid="+ umpAppId + "&secret=" + umpSecret + "&grant_type=client_credential";
            String url = "https://api.weixin.qq.com/cgi-bin/token?appid=wxa2cece345fbbc2ff&secret=ffd369a1c497eaa73ff38902a45a0efc&grant_type=client_credential";
            try {
                String res = SendHttpRequestUtil.get(url);
                UserMpAccessToken userToken = JSONUtil.toBean(res,UserMpAccessToken.class);

                if (userToken.access_token != null) {
                    userMpAccessToken = userToken.access_token;
                    userMpAccessTokenExpire = userToken.expires_in * 1000;
                } else {
                    getUserMpAccessToken();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userMpAccessToken;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserMpAccessToken {
        /**
         {
         "access_token": "60_UfbNMx3Sv1N9ocARzY9GLdInwTXtZakCuLZaRg5zfhnxOEYqXeMR9qbG_j61vlzwiP01K0LOHwoTbvWeH3-OOBNiKo8LdWruMpSiVAYukSVwEP8eLEkQ27WJqA1M4PxEtM1I6rGl-c31cfc_SBRjACAIRH",
         "expires_in": 7200
         }
         */
        private String access_token;
        private long expires_in;
    }


    /**
     * 小程序模板信息
     */
    public static SendSubscribeMessageResponse sendMpSubscribeMessage(SendSubscribeMessageParams data) {
        SendSubscribeMessageResponse mResponse = null;
        String access_token = getUserMpAccessToken();
        if (access_token != null) {
            String url = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + access_token;
            try {
                String res = SendHttpRequestUtil.post(url,JSONUtil.toJsonStr(data).getBytes());
                mResponse = JSONUtil.toBean(res,SendSubscribeMessageResponse.class);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mResponse;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendSubscribeMessageParams {
        private String touser;
        private String template_id;
        private String page;
        private Map<String,Object> data;
        private String lang = "zh_CN";
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendSubscribeMessageResponse {
        private int errcode;
        private String errmsg;
    }


    public static void main(String[] args) {
        WeChat.getUserMpAccessToken();
    }


}
