package cn.chowa.ejyy.option;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @ClassName building
 * @Description TODO
 * @Author ironman
 * @Date 11:04 2022/8/23
 */
@RestController
@RequestMapping("/pc/option")
public class building {


    @Autowired
    private BuildingQuery buildingQuery;

    @SaCheckRole(Constants.RoleName.ANYONE)
    @VerifyCommunity(true)
    @PostMapping("/building")
    public Map<String,Object> building(@RequestBody RequestData data) {
        long communityId = data.getCommunityId();
        List<ObjData> list = buildingQuery.getBuildingInfo(communityId);
        return Map.of(
                "list",list
        );
    }
}
