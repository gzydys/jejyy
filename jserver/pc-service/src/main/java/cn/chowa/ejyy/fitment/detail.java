package cn.chowa.ejyy.fitment;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.FitmentQuery;
import cn.chowa.ejyy.dao.PropertyCompanyUserRepository;
import cn.chowa.ejyy.model.entity.PropertyCompanyUser;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;
import static cn.chowa.ejyy.common.Constants.fitment.PROPERTY_COMPANY_ALLOW_STEP;
import static cn.chowa.ejyy.common.Constants.fitment.PROPERTY_COMPANY_CONFIRM_STEP;

@RestController("fitmentDetail")
@RequestMapping("/pc/fitment")
public class detail {

    @Autowired
    private FitmentQuery fitmentQuery;
    @Autowired
    private PropertyCompanyUserRepository propertyCompanyUserRepository;

    /**
     * 装修 - 详情
     */
    @SaCheckRole(Constants.RoleName.ZXDJ)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<String, Object> detail(@RequestBody RequestData data) {
        int id = data.getInt("id", true, "^\\d+$");
        ObjData agreeUserInfo = new ObjData();
        ObjData confirmUserInfo = new ObjData();
        ObjData returnUserInfo = new ObjData();

        ObjData info = fitmentQuery.getFitmentDetail(data.getCommunityId(), id);
        if (info == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法获取装修登记信息");
        }

        int step = info.getInt("step");
        if (step >= PROPERTY_COMPANY_ALLOW_STEP) {
            PropertyCompanyUser user = propertyCompanyUserRepository.findById(info.getLong("agree_user_id")).get();
            agreeUserInfo = new ObjData(Map.of(
                    "id", user.getId(),
                    "real_name", user.getRealName()
            ));
        }

        if (step == PROPERTY_COMPANY_CONFIRM_STEP) {
            PropertyCompanyUser user = propertyCompanyUserRepository.findById(info.getLong("confirm_user_id")).get();
            confirmUserInfo = new ObjData(Map.of(
                    "id", user.getId(),
                    "real_name", user.getRealName()));

            if (info.getLong("return_operate_user_id") > 0) {
                user = propertyCompanyUserRepository.findById(info.getLong("return_operate_user_id")).get();
                returnUserInfo = new ObjData(Map.of(
                        "id", user.getId(),
                        "real_name", user.getRealName()
                ));
            }
        }

        info.remove("agree_user_id");
        info.remove("confirm_user_id");
        info.remove("return_operate_user_id");
        return Map.of(
                "info", info,
                "agreeUserInfo", agreeUserInfo,
                "confirmUserInfo", confirmUserInfo,
                "reutrnUserInfo", returnUserInfo
        );
    }

}
