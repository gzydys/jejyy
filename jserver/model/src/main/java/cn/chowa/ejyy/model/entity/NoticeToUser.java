package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_notice_to_user")
public class NoticeToUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

    private String overview;

    private String content;

    @JsonProperty("community_id")
    private Long communityId;

    /**
     * 1 物业公司 2 系统
     */
    private int refer;

    @JsonProperty("notice_tpl_id")
    private Long noticeTplId;

    private Boolean published;

    @JsonProperty("published_at")
    private Long publishedAt;

    @JsonProperty("published_by")
    private Long publishedBy;

    @JsonProperty("created_by")
    private long createdBy;

    @JsonProperty("created_at")
    private long createdAt;
}
