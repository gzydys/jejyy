package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.UserBuildingOperateLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserBuildingOperateLogRepository extends JpaRepository<UserBuildingOperateLog, Long> {
}
