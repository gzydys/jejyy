package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.WechatMpAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatMpAuthRepository extends JpaRepository<WechatMpAuth, Long> {

    WechatMpAuth findByWechatMpUserId(long wechatMpUserId);

}
