package cn.chowa.ejyy.dao.question;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface QuestionQuery {

    List<ObjData> getQuestionnaireList(int published, long community_id, int page_size, int page_num);

    long getQuestionnaireListCount(int published, long community_id);


}
