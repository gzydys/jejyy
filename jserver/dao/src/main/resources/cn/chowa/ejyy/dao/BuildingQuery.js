function getBindingStatus(id, buildingId, communityId){
    var sql=`
        select a.status from ejyy_user_building a
        left join ejyy_building_info b on a.building_id=b.id
        where a.id=:id and a.building_id=:building_id and b.community_id=:communityId
    `;

    return sql;
}

function getBuildingOwner(buildingId){
    var sql=`
        select  a.id,b.id as user_id,b.real_name,a.status
        from ejyy_user_building a left join ejyy_wechat_mp_user b on a.wechat_mp_user_id=b.id
        where a.building_id=:buildingId
    `;

    return sql;
}

function getBuildingHistoryOperations(userBuildingId){
    var sql=`
        select
            a.status,a.operate_by,a.created_at,
            b.id as ejyy_wechat_mp_user_id
            b.real_name as ejyy_wechat_mp_user_real_name,
            c.id as property_company_user_id,
            c.real_name as property_company_user_real_name
        from ejyy_user_building_operate_log a left join ejyy_wechat_mp_user b on a.wechat_mp_user_id=b.id
        left join ejyy_property_company_user c on a.property_company_user_id=c.id
        where a.user_building_id=:userBuildingId
        order by id desc
    `;

    return sql;
}

function getBindingTotal(communityId,type,buildingStatus){
    var sql=`
        select count(*)
        from ejyy_building_info a where a.community_id=:communityId
        and type=:type
        and a.id in(
            select b.building_id
            from ejyy_user_building b left join ejyy_building_info c on b.building_id=c.id
            where c.community_id=:communityId and c.type=:type and b.status=:buildingStatus
        )
    `;

    return sql;
}

function getOwnerTotal(communityId, buildingStatus){
  var sql=`
        select count(*)
        from ejyy_wechat_mp_user a where
        a.id in(
            select b.wechat_mp_user_id
            from ejyy_user_building b left join ejyy_building_info c on b.building_id=c.id
            where c.community_id=:communityId and b.status=:buildingStatus
        )
    `;

    return sql;
}

function getCarTotal(status,communityId,type){
    var sql=`
        select count(*)
        from ejyy_user_car a where a.status=:status and
        a.building_id in(
            select b.id
            from ejyy_building_info b where b.community_id=:communityId and b.type=:type
        )
    `;

    return sql;
}

function getOwnerBuildings(ownerId, status, community_id){
    var sql=`
        select
            a.id as user_building_id,
            a.authenticated,
            a.authenticated_type,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number,
            b.id as building_id
        from ejyy_user_building a left join ejyy_building_info b on a.building_id=b.id
        where a.wechat_mp_user_id=:ownerId and a.status=:status and
        b.community_id=:community_id
        order by a.id desc
    `;

    return sql;
}

function getUserBuildingInfo( wechat_mp_user_id,  building_id,  community_id){
    var sql=`
        select *
        from ejyy_user_building a left join ejyy_building_info b on a.building_id=b.id
        where a.wechat_mp_user_id=:wechat_mp_user_id and a.building_id=:building_id
        and b.community_id=:community_id
    `;

    return sql;
}

function getWechatCommunityUserBuilding(status,wechat_mp_user_id){
    return `
        select
            a.id as user_building_id,
            a.authenticated,
            a.authenticated_type,
            a.id as building_id,
            b.community_id,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number,
            c.name,
            c.banner,
            c.phone,
            c.province,
            c.city,
            c.district,
            d.access_nfc,
            d.access_qrcode,
            d.access_remote,
            d.fitment_pledge
        from ejyy_user_building a left join ejyy_building_info b on a.building_id=b.id
            left join ejyy_community_info c on b.community_id=c.id
            left join ejyy_community_setting d on b.community_id=d.community_id
        where a.wechat_mp_user_id=:wechat_mp_user_id and a.status=:status
        order by c.id desc
    `;
}

function getBuildingInfo(community_id) {
    var sql = `
        select 
            a.id as building_id,
            a.type,
            a.area,
            a.building,
            a.unit,
            a.number
        from ejyy_building_info a 
        where a.community_id=:community_id
    `;
    return sql;
}

function getBuildingList(community_id,wechat_mp_user_id,content) {
    var sql = `
        select 
            b.id,
            b.building_id,
            a.type,
            a.area,
            a.building,
            a.unit,
            a.number,
            a.construction_area,
            a.created_at,
            b.authenticated,
            b.authenticated_type,
            b.status  
        from ejyy_building_info a 
        left join ejyy_user_building b on b.building_id = a.id
        where a.community_id=:community_id and b.wechat_mp_user_id=:wechat_mp_user_id
        and a.id in ${content}
    `;
    return sql;
}


function getBuildInfo(building_ids) {
    var sql = `
        select a.id from ejyy_building_info a
        left join ejyy_user_building b on b.building_id = a.id
        where a.id in ${building_ids} and b.building_id is not null        
    `;
    return sql;
}


function getBuildings(community_id,wechat_mp_user_id,bids) {
    var sql = `
        select
            b.id,
            b.building_id,
            a.type,
            a.area,
            a.building,
            a.unit,
            a.number,
            a.construction_area,
            a.created_at,
            b.authenticated,
            b.authenticated_type,
            b.status
        from ejyy_building_info a
        left join ejyy_user_building b on b.building_id = a.id
        where a.community_id =:community_id and b.wechat_mp_user_id=:wechat_mp_user_id
        and a.id in ${bids}
    `;
    return sql;
}

function getBindingSetting(building_id,community_id){
    var sql=`
        select
            b.carport_max_car,
            b.garage_max_car,
            a.type
        from ejyy_building_info a left join ejyy_community_setting b on a.community_id=b.community_id
        where a.id=:building_id and a.community_id=:community_id
    `;
    return sql;
}