function getCommunitySettingInfo(userId){
    var sql=`
        select
            b.community_id,b.access_nfc,b.access_qrcode,
            b.access_remote,b.fitment_pledge,a.name
        from ejyy_community_info a left join ejyy_community_setting b on a.id=b.community_id
        where a.id in(
            select community_id from ejyy_property_company_user_access_community
            where property_company_user_id=:userId
        )
    `;

    return sql;
}

function getDepartmentJob(userId){
    var sql=`
        select b.name as department,c.name as job
        from ejyy_property_company_user a left join ejyy_property_company_department b on a.department_id=b.id
        left join ejyy_property_company_job c on a.job_id=c.id
        where a.id=:userId
    `;

    return sql;
}